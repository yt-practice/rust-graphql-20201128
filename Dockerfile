# ------------------------------------------------------------------------------
# Cargo Build Stage
# ------------------------------------------------------------------------------
FROM ekidd/rust-musl-builder:latest as cargo-build
USER rust
COPY Cargo.toml Cargo.toml
COPY Cargo.lock Cargo.lock
RUN mkdir src
RUN echo "fn main() { println!(\"if you see this, the build broke\"); std::process::exit(1); }" > src/main.rs
RUN cargo build --release
RUN rm -f target/x86_64-unknown-linux-musl/release/deps/main*
COPY . .
RUN cargo build --release

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------
FROM scratch
WORKDIR /home/app/bin/
COPY --from=cargo-build /home/rust/src/target/x86_64-unknown-linux-musl/release/main .
ENTRYPOINT ["./main"]
