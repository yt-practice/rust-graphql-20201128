use actix_web::{get, post, web, Error, HttpResponse as Response, Responder};
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;

use crate::schema::Schema;

#[get("/")]
pub async fn health() -> impl Responder {
	Response::Ok()
		.content_type("text/html; charset=utf-8")
		.body("Ok.")
}

#[get("/graphiql")]
pub async fn graphiql() -> impl Responder {
	let html = graphiql_source("/graphql");
	Response::Ok()
		.content_type("text/html; charset=utf-8")
		.body(html)
}

#[post("/graphql")]
pub async fn graphql(
	st: web::Data<Schema>,
	data: web::Json<GraphQLRequest>,
) -> Result<impl Responder, Error> {
	let res = data.execute(&st, &());
	let result = serde_json::to_string(&res)?;
	Ok(Response::Ok().content_type("application/json").body(result))
}
