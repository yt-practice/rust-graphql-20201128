use chrono::prelude::*;

#[derive(Debug, Clone, Queryable)]
pub struct ToDo {
	pub id: i32,
	pub text: Option<String>,
	pub created_at: NaiveDateTime,
}

use super::schema::todos;

#[derive(Debug, Clone, Insertable)]
#[table_name = "todos"]
pub struct NewToDo {
	pub text: String,
}
