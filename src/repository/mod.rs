pub mod conn;
pub mod models;
pub mod schema;

mod error;

use crate::entity::todo::{BoxedError, Repository, Todo};
pub use error::Error;

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

use std::sync::{Arc, Mutex};

pub struct Connection {
	pub pool: Arc<Mutex<SqliteConnection>>,
}

impl Connection {
	pub fn new() -> Connection {
		Connection {
			pool: Arc::new(Mutex::new(conn::establish_connection())),
		}
	}
}

fn to_view(d: &models::ToDo) -> Todo {
	Todo {
		id: d.id,
		text: d
			.text
			.as_ref()
			.map(|s| s.into())
			.unwrap_or_else(|| "".into()),
		created_at: d.created_at,
	}
}

fn find_one(conn: &SqliteConnection, id: i32) -> Result<Todo, BoxedError> {
	let item: models::ToDo = dsl::todos
		.find(id)
		.select((dsl::id, dsl::text, dsl::created_at))
		.get_result(conn)?;
	Ok(to_view(&item))
}

use schema::todos::dsl;

impl Repository for Connection {
	fn list(&self) -> Result<Vec<Todo>, BoxedError> {
		let conn = self.pool.lock().map_err(|_| "err")?;
		let list: Vec<models::ToDo> = dsl::todos
			.select((dsl::id, dsl::text, dsl::created_at))
			.get_results(&*conn)?;
		Ok(list.iter().map(to_view).collect())
	}

	fn fetch(&self, id: i32) -> Result<Todo, BoxedError> {
		let conn = self.pool.lock().map_err(|_| "err")?;
		find_one(&*conn, id)
	}

	fn create(&self, content: &str) -> Result<i32, BoxedError> {
		let new_todo = models::NewToDo {
			text: content.into(),
		};
		let conn = self.pool.lock().map_err(|_| "err")?;
		diesel::insert_into(dsl::todos)
			.values(&new_todo)
			.execute(&*conn)?;
		let id = dsl::todos
			.select(dsl::id)
			.filter(dsl::text.eq(content))
			.order(dsl::created_at.desc())
			.first::<i32>(&*conn)?;
		Ok(id)
	}

	fn update(&self, id: i32, content: &str) -> Result<Todo, BoxedError> {
		let conn = self.pool.lock().map_err(|_| "err")?;
		diesel::update(dsl::todos.find(id))
			.set(dsl::text.eq(content))
			.execute(&*conn)?;
		find_one(&*conn, id)
	}

	fn delete(&self, id: i32) -> Result<(), BoxedError> {
		let conn = self.pool.lock().map_err(|_| "err")?;
		diesel::delete(dsl::todos.find(id)).execute(&*conn)?;
		Ok(())
	}
}
