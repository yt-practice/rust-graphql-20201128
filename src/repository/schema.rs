table! {
    todos (id) {
        id -> Integer,
        text -> Nullable<Text>,
        created_at -> Timestamp,
    }
}
