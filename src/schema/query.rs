use juniper::{graphql_value, FieldError, FieldResult, Value};
use std::sync::Arc;

use crate::entity::todo::{Repository, Todo};

pub struct Query {
	repository: Arc<dyn Repository>,
}

impl Query {
	pub fn new(repository: Arc<dyn Repository>) -> Self {
		Self { repository }
	}

	fn todos(&self) -> FieldResult<Vec<Todo>> {
		self
			.repository
			.list()
			.map_err(|e| FieldError::new(&format!("{}", e), Value::Null))
	}

	fn todo(&self, id: i32) -> FieldResult<Todo> {
		self
			.repository
			.fetch(id)
			.map_err(|e| FieldError::new(e, graphql_value!({ "id": id })))
	}
}

#[juniper::object]
impl Query {
	fn hello() -> String {
		"world".into()
	}
	fn todos(&self) -> FieldResult<Vec<Todo>> {
		self.todos()
	}

	fn todo(&self, id: i32) -> FieldResult<Todo> {
		self.todo(id)
	}
}
