use juniper::{graphql_value, FieldError, FieldResult};
use std::sync::Arc;

use crate::entity::todo::{Repository, Todo};

pub struct Mutation {
	repository: Arc<dyn Repository>,
}

impl Mutation {
	pub fn new(repository: Arc<dyn Repository>) -> Self {
		Self { repository }
	}

	fn register(&self, text: String) -> FieldResult<Todo> {
		self
			.repository
			.create(&text)
			.and_then(|id| self.repository.fetch(id))
			.map_err(|e| FieldError::new(e.to_string(), graphql_value!({ "text": text })))
	}

	fn update(&self, id: i32, text: String) -> FieldResult<Todo> {
		self
			.repository
			.update(id, text.as_str())
			.map_err(|e| FieldError::new(e, graphql_value!({ "id": id, "text": text })))
	}

	fn delete(&self, id: i32) -> FieldResult<i32> {
		self
			.repository
			.delete(id)
			.map(|_| id)
			.map_err(|e| FieldError::new(e, graphql_value!({ "id": id })))
	}
}

#[juniper::object]
impl Mutation {
	fn register(&self, text: String) -> FieldResult<Todo> {
		self.register(text)
	}

	fn update(&self, id: i32, text: String) -> FieldResult<Todo> {
		self.update(id, text)
	}

	fn delete(&self, id: i32) -> FieldResult<i32> {
		self.delete(id)
	}
}
