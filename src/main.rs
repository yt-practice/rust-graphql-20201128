#[macro_use]
extern crate diesel;
extern crate dotenv;

mod controller;
mod entity;
mod repository;
mod schema;

use actix_web::{web, App, HttpServer};
use std::sync::Arc;

fn config(cfg: &mut web::ServiceConfig) {
  cfg
    .service(controller::health)
    .service(controller::graphiql)
    .service(controller::graphql);
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
  let repository = Arc::new(repository::Connection::new());

  let schema = web::Data::new(schema::create_schema(repository.clone()));

  println!("start on 8000");

  HttpServer::new(move || App::new().app_data(schema.clone()).configure(config))
    .bind("0.0.0.0:8000")?
    .run()
    .await
}
