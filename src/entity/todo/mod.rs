use chrono::prelude::NaiveDateTime;
use std::marker::{Send, Sync};

// pub mod error;
// pub mod id;

// pub use error::Error;
// pub use id::TodoId;

#[derive(Clone, Debug, PartialEq, juniper::GraphQLObject)]
pub struct Todo {
	pub id: i32,
	pub text: String,
	pub created_at: NaiveDateTime,
}

pub type BoxedError = Box<dyn std::error::Error>;

pub trait Repository: Sync + Send {
	fn list(&self) -> Result<Vec<Todo>, BoxedError>;
	fn fetch(&self, id: i32) -> Result<Todo, BoxedError>;
	fn create(&self, text: &str) -> Result<i32, BoxedError>;
	fn update(&self, id: i32, text: &str) -> Result<Todo, BoxedError>;
	fn delete(&self, id: i32) -> Result<(), BoxedError>;
}
