# todos

todo を管理する graphql サーバを Rust で実装してみた

# setup

```shell
cp .env_default .env
diesel database setup
diesel migration run
```

# build & run

```shell
cargo build --release
./target/release/main
```

# ルーティング

- `GET /` 疎通確認用
- `GET /graphiql` graphql を叩いてみるデモ用 UI
- `POST /graphql` graphql

# dev

```shell
cargo watch -x run
```

# 参考

https://github.com/igm50/rust-graphql-sample
