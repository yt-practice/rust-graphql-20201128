-- Your SQL goes here
CREATE TABLE todos (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	text TEXT NULL,
	created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
)